<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function wp_feeds_wxr_importer_taxonomy_default_vocabularies() {
  return array(
    'wp_categories' => array(
      'name' => 'WordPress categories',
      'description' => 'The <em>WordPress Importer</em> module imports WordPress categories to this vocabulary.',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_wp_categories',
      'weight' => '0',
      'nodes' => array(
        'wp_blog' => 'wp_blog',
      ),
    ),
    'wp_tags' => array(
      'name' => 'WordPress tags',
      'description' => 'The <em>WordPress Importer</em> module imports WordPress tags to this vocabulary.',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '1',
      'module' => 'features_wp_tags',
      'weight' => '0',
      'nodes' => array(
        'wp_blog' => 'wp_blog',
      ),
    ),
  );
}
