<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function wp_feeds_wxr_importer_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
  elseif ($module == "wp_feeds" && $api == "wp_feeds_parser_default") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function wp_feeds_wxr_importer_node_info() {
  $items = array(
    'wp_blog' => array(
      'name' => t('WordPress blog post'),
      'module' => 'features',
      'description' => t('The <em>WordPress Importer</em> module imports items from WordPress WXR files (WordPress extensible RSS) to this content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
