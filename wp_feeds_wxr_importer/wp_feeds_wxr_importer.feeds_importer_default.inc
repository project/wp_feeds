<?php

/**
 * Implementation of hook_feeds_importer_default().
 */
function wp_feeds_wxr_importer_feeds_importer_default() {
  $export = array();
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'wordpress_xml';
  $feeds_importer->config = array(
    'name' => 'WordPress XML',
    'description' => 'Imports XML exports from wordpress.com sites (also known as WordPress eXtended RSS or WXR)',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsWordpressXmlParser',
      'config' => array(
        'create_terms' => array(
          'category' => 0,
          'tag' => 0,
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'wp_blog',
        'input_format' => '2',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'url',
            'target' => 'url',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'timestamp',
            'target' => 'created',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'is_published',
            'target' => 'status',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'wp_is_sticky',
            'target' => 'sticky',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'wp_comment_status',
            'target' => 'comments_status',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'path',
            'target' => 'path',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'content',
            'target' => 'body',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'content_teaser',
            'target' => 'field_wp_teaser',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'content_body',
            'target' => 'field_wp_body',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'wp_post_id',
            'target' => 'field_wp_post_id',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'author_name',
            'target' => 'username',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'author_name',
            'target' => 'field_wp_author',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'tags',
            'target' => 'taxonomy:features_wp_tags',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'categories',
            'target' => 'taxonomy:features_wp_categories',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'wp_attachments',
            'target' => 'field_wp_files',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'meta_enclosure',
            'target' => 'field_wp_enclosure',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'comments',
            'target' => 'comments',
            'unique' => FALSE,
          ),
        ),
        'author' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );

  $export['wordpress_xml'] = $feeds_importer;
  return $export;
}
