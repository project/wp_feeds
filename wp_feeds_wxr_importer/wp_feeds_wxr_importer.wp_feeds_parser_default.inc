<?php

/**
 * Implementation of hook_wp_feeds_parser_default().
 */
function wp_feeds_wxr_importer_wp_feeds_parser_default() {
  $export = array();
  $wp_feeds_parser = new stdClass;
  $wp_feeds_parser->disabled = FALSE; /* Edit this to true to make a default wp_feeds_parser disabled initially */
  $wp_feeds_parser->api_version = 1;
  $wp_feeds_parser->id = 'wordpress_xml';
  $wp_feeds_parser->config = array(
    'create_terms' => array(
      'category' => '1',
      'tag' => '0',
    ),
  );

  $export['wordpress_xml'] = $wp_feeds_parser;
  return $export;
}
