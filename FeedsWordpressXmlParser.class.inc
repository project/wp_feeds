<?php
// $Id$

/**
 * @file
 * Class definition for FeedsWordpressXmlParser and FeedsWordpressEnclosure.
 */


/**
 * DTD for the WP XMLNS;
 *
 * DTD: Document Type Definition
 * WP: WordPress
 * XMLNS: XML NameSpace
 * XML: eXtensible Markup Language
 *
 * In otherwords, this is the proper way to refer to WordPress XML
 * tags/elements like <wp:post_id> and <wp:comment>.
 */
define('WP_FEEDS_XMLNS', 'http://wordpress.org/export/1.0/');


/**
 * Extends FeedsSimplePie parser to override & extend most of it.
 */
class FeedsWordpressXmlParser extends FeedsSimplePieParser {
  /**
   * Constructor, set id and load default configuration.
   */
  protected function __construct($id) {
    parent::__construct($id);
    $this->loadConfig();
  }

  protected function loadConfig() {
    // Make sure configuration is populated.
    ctools_include('export');
    $result = ctools_export_load_object('wp_feeds_parser', 'conditions', array('id' => $this->id));
    if ($result) {
      $result = array_shift($result);
      $this->export_type = $result->export_type;
      $this->disabled = isset($result->disabled) ? $result->disabled : FALSE;
      $this->config = $result->config;
    }
  }

  public function save() {
    $save = new stdClass();
    $save->id = $this->id;
    $save->config = $this->getConfig();
    if (db_result(db_query_range("SELECT 1 FROM {wp_feeds_parser} WHERE id = '%s'", $this->id, 0, 1))) {
      drupal_write_record('wp_feeds_parser', $save, 'id');
    }
    else {
      drupal_write_record('wp_feeds_parser', $save);
    }
  }

  /**
   * Return default configuration.
   *
   * @todo Settings:  Implement settings for @todos marked "settings:".
   * @todo Feeds:  Rename to getConfigDefaults().
   *
   * @return
   *   Array where keys are the variable names of the configuration elements and
   *   values are their default values.
   */
  public function configDefaults() {
    return array(
      'create_terms' => array(
        'category' => 0,
        'tag' => 0,
      ),
    );
  }

  /**
   * Return configuration form for this object. The keys of the configuration
   * form must match the keys of the array returned by configDefaults().
   *
   * @return
   *   FormAPI style form definition.
   */
  public function configForm(Array &$form_state) {
    $form = array();

    $description = array(
      t('Creates taxonomy terms for <strong>all</strong> WordPress categories or tags.'),
      t('It is usually desirable to configure the Feeds Processor Mapping so that the "<em>WP: Categories</em>" and "<em>WP: Tags</em>" sources to map to the same taxonomy vocabulary as the vocabulary selected here.'),
      t('Note that for "<em>tagging</em>" vocabularies, Drupal taxonomy terms will be created anyway (regardless of this setting) if the WordPress category or tag is referenced in the WordPress post.  However this setting is useful if WordPress categories or tags that are <em>not</em> referenced by any WordPress posts need to be imported.'),
      t('Heirarchy of WordPress categories is not currently supported.')
    );
    foreach (array_keys($description) as $key) {
      $description[$key] = '<p>' . $description[$key] . '</p>';
    }

    $form['create_terms'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Import taxonomy terms'),
      '#description' => implode("\n", $description),
    );

    $options = array();
    foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
      $configuration = array();
      if ($vocabulary->tags) {
        $configuration[] = '<em>' . t('tagging') . '</em>';
      }
      $configuration[] = $vocabulary->multiple ? t('multiple') : t('single');
      if ($vocabulary->required) {
        $configuration[] = t('required');
      }
      if ($vocabulary->hierarchy) {
        $configuration[] = t('hierarchical');
      }
      $configuration = implode(', ', $configuration);
      $options[$vid] = t('<strong>!name:</strong> !configuration', array('!name' => $vocabulary->name, '!configuration' => $configuration));
    }

    $this->loadConfig();

    $domains = array(
      'category' => t('categories'),
      'tag' => t('tags'),
    );
    foreach ($domains as $domain => $name) {
      $t_vars = array('!domain' => $name);
      $option_none = array(0 => t('<strong>None:</strong> Only create Drupal taxonomy terms for WordPress !domain if they are referenced in WordPress posts.', $t_vars));

      $form['create_terms'][$domain] = array(
        '#type' => 'radios',
        '#required' => TRUE,
        '#title' => t('WordPress !domain vocabulary', $t_vars),
        '#description' => t('The vocabulary for Drupal taxonomy terms created for WordPress !domain', $t_vars),
        '#options' => $option_none + $options,
        '#default_value' => $this->config['create_terms'][$domain],
      );
    }

    return $form;
  }

  /**
   * Add the extra mapping sources provided by this parser.
   */
  public function getMappingSources() {
    $elements = parent::getMappingSources();

    // WordPress WRX files do not appear to use these elements.
    $unused_sources = array('description', 'domains', 'author_link', 'author_email', 'enclosures', 'location_latitude', 'location_longitude');
    foreach ($unused_sources as $source) {
      unset($elements[$source]);
    }
    $elements['timestamp']['name'] = t('Post date (unix timestamp)');

    $names = array(
      // The names (the array keys) are used to programmatically generate
      // machine names for the elements.
      '' => array(
        // Elements that have a specific processor to get Drupal-specific data.
        t('Content')          => t('The whole content body of the WordPress post.'),
        t('Content teaser')   => t("The first part of the WordPress post's content body, as determined by either the WordPress <!--more--> tag or drupal_teaser()."),
        t('Content body')     => t('The rest of the content body without the teaser.'),
        t('Is published')     => t('If the WordPress item is published.'),
        t('Path')             => t('The path of the remote item.  This is useful for content-migration.  E.g. 2010/11/15/what-are-you-solving'),
        // Multi-valued elements.
        t('Categories')       => t('Categories of a wordpress element'),
        t('Tags')             => t('Tags on a WordPress element'),
        t('Comments')         => t('Comments on the WordPress item.'),
        t('Trackbacks')       => t('Pingbacks on the WordPress item.'),
      ),
      'wp_' => array(
        // Elements for generic and/or raw wordpress data.
        t('Post ID')          => t('Unique ID of the WordPress item.  E.g. A serial integer'),
        t('Post date')        => t('Date the WordPress item was posted.  E.g. 2010-12-14 13:38:02  (Neither WRX files nor the Post date state the timezone of the WordPress site.)'),
        t('Post date (UTC)')  => t('Date the WordPress item was posted in UTC (Univeral Coordinated Time).  E.g. 2010-12-14 19:38:02 (WRX files to not set the UTC date for items that are not published.)'),
        t('Comment status')   => t('If comments are enabled.'),
        t('Ping status')      => t('If pings are enabled.'),
        t('Post name')        => t('The machine name of the WordPress item.  It is either empty or unique, but not both empty and unique.  E.g. drupal-rocks-the-foos-bar'),
        t('Status')           => t('Status of the WordPress item.  E.g. draft, publish, auto-draft, schedule, inherit, trash'),
        t('Post type')        => t('The type of WordPress item.  E.g. post, attachment, page, safecss'),
        t('Menu order')       => t('The weight of the WordPress item in menus.  E.g. A serial integer'),
        t('Post password')    => t('Password of the WordPress item.  An individual item can be password protected.'),
        t('Is sticky')        => t('If the WordPress item is sticky.'),
        // @todo Major:  Make the "Post parent" relationship synchronizable as
        // a nodereference:  Save a map of WordPress Post IDs to Drupal Node
        // IDs and map the parent relationship after Feeds set-target
        // callbacks are done and nodes are saved.
        // @see wp_feeds_feeds_node_processor_targets_set_comments()
        t('Post parent')      => t('The parent post of the WordPress item.  Items of type attachment reference the post ID of the post to which the attachment belongs.'),
        t('Attachments')      => t('The attachments to the WordPress item.  E.g. http://myblog.files.wordpress.com/2010/03/myimage.jpg'),
        t('Attachment URL')   => t('The URL of the attachment, for items of type attachment. E.g. http://myblog.files.wordpress.com/2010/03/myimage.jpg'),
      ),
      'meta_' => array(
        // Elements for wordpress meta data elements.
        // <wp:post_meta> contains meta data relating to the post, including;
        // enclosure, geo_latitude, geo_longitude, geo_accuracy, geo_address,
        // geo_public, jabber_published, email_notification,
        // _wp_attachment_metadata, _edit_last, _edit_lock.
        t('Enclosure')           => t('The enclosure attached to the WordPress item.  E.g. http://myblog.files.wordpress.com/2010/03/mypodcast.mp3'),
        t('Attachment meta-data') => t('Serialized array of meta data relating to the attachment, such as shutter-speed and file size.'),
        t('Geo: Latitude')        => t('Latitude for the WordPress item.'),
        t('Geo: Longitude')       => t('Longitude for the WordPress item.'),
        t('Geo: Accuracy')        => t('Accuracy of the latitude and longitude of this WordPress item.'),
        t('Geo: Address')         => t('Geographical address of the WordPress item.'),
        t('Geo: Public')          => t('Whether or not the location of the WordPress item is Public.'),
        t('Edit lock date')       => t('Timestamp of the lock on the WordPress item.'),
      ),
    );

    // Build machine names and integrate into $elements.
    foreach ($names as $prefix => $items) {
      foreach ($items as $name => $description) {
        // Parse the human readable name into a machine name.  E.g. 'Post ID'
        // becomes 'post_id'.
        $replacements = array(
          ' ' => '_',
          // GMT is an out-dated synonym for UTC.  WRX files use "gmt".
          // wp_feeds uses "UTC" in the UI.
          'UTC' => 'GMT',
          // Remove punctuation.
          '-' => '',
          ':' => '',
          '(' => '',
          ')' => '',
        );
        // Substitute the replacements to get the key.
        $key = strtolower(str_replace(array_keys($replacements), $replacements, $name));
        $elements[$prefix . $key] = compact('name', 'description');
      }
    }

    // Sort the elements alphabetically.
    asort($elements);
    return $elements;
  }

  /**
   * Implementation of FeedsParser::parse().
   *
   * @see FeedsSimplePieParser::parse()
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    feeds_include_library('simplepie.inc', 'simplepie');

    // Initialize SimplePie.
    $parser = new SimplePie();
    $parser->set_raw_data($batch->getRaw());
    $parser->set_stupidly_fast(TRUE);
    $parser->encode_instead_of_strip(FALSE);
    // @todo Feeds:  Is caching effective when we pass in raw data?
    $parser->enable_cache(TRUE);
    $parser->set_cache_location($this->cacheDirectory());
    $parser->init();

    if ($parser->error) {
      drupal_set_message($parser->error, 'error');
      drupal_set_message(t('Try deleting lines in the file that start with <code>&lt;atom:link</code>.'), 'warning');
      return;
    }

    // Construct the standard form of the parsed feed
    $batch->title = html_entity_decode(($title = $parser->get_title()) ? $title : $this->createTitle($parser->get_description()));
    $batch->description = $parser->get_description();
    $batch->link = html_entity_decode($parser->get_link());

    // Parse out the WordPress categories and tags first.
    $this->createTaxonomyTerms($parser);

    // Prepare to parse out the feed items.
    $this->simplepie_items = $parser->get_items();
    $this->feeds_source_items = array();
    $this->attachment_items = array();

    // Iterate over the items.
    foreach (array_keys($this->simplepie_items) as $this->current_item_index) {
      if ($item = $this->parseItem()) {
        $post_id = $item['wp_post_id'];
        $this->feeds_source_items[$post_id] = $item;
      }
    }

    // Release parser (free-up memory) and tidy up.
    unset($parser);
    $this->attachAttachmentPosts();
    $batch->items = $this->feeds_source_items;

    // This is a handy method to debug the parsed elements.
    if (function_exists('dpm')) {
      dpm($this->feeds_source_items, 'Parsed items');
    }
  }

  /**
   * Creates Drupal taxonomy terms for WordPress categories and terms.
   *
   * This allows Feeds taxonomy vocabulary targets to resolve term names for
   * vocabularies that are not tagging vocabularies.
   *
   * @param $parser SimplePie
   *    The SimplePie parser for the WordPress WXR feed file.
   */
  protected function createTaxonomyTerms(SimplePie $parser) {
    $this->loadConfig();

    $name_tags = array(
      'category' => 'cat_name',
      'tag' => 'tag_name',
    );
    feeds_include('taxonomy', 'mappers');
    foreach ($this->config['create_terms'] as $tag => $vid) {
      if ($vid > 0) {
        $new = $exist = 0;
        foreach ($parser->get_channel_tags(WP_FEEDS_XMLNS, $tag) as $element) {
          $name_tag = $name_tags[$tag];
          if ($name = $this->cleanTaxonomyTerm($element['child'][WP_FEEDS_XMLNS][$name_tag][0]['data'])) {
            // @todo Major:  Support WordPress category hierarchy:  Get
            // <wp:category_parent> & import it in a second pass.
            $terms = taxonomy_get_term_by_name_vid($name, $vid);
            if (empty($terms)) {
              $new++;
              taxonomy_save_term(compact('name', 'vid'));
            }
            else {
              $exist++;
            }
          }
        }
        if ($new || $exist) {
          $vocabulary = taxonomy_vocabulary_load($vid);
          // @todo Minor: drupal_set_message() does not work for first batches.
          // @see http://drupal.org/node/1019408
          drupal_set_message(t('%new taxonomy terms were created in the %name vocabulary.  %exist already existed.  (One for each WordPress !domain.)', array('%new' => $new, '%name' => $vocabulary->name, '%exist' => $exist, '!domain' => $tag)));
        }
      }
    }
  }

  /**
   * Cleans a taxonomy term before saving it.
   *
   * Trims redundant wrapping characters and decodes HTML entities.
   *
   * @param $term String
   *    The term string to clean.
   * @return String
   *    The cleaned string.
   */
  protected function cleanTaxonomyTerm($term) {
    if ($term == 'Uncategorized') {
      return FALSE;
    }
    // SimplPie re-encodes content that is already encoded by WordPress.
    // Decode it twice so that "&amp;amp;" resolves to "&".
    return trim(html_entity_decode(html_entity_decode($term)), '\'" ,');
  }

  /**
   * Handles orphaned attachments & moves attachment items into parent posts.
   */
  protected function attachAttachmentPosts() {
    foreach ($this->attachment_items as $post_id => $attachments) {
      if (!isset($this->feeds_source_items[$post_id])) {
        // @todo Settings:  Handling of orphaned attachments.
        // The parent post of the attachment is missing.  Save files to the
        // files table anyway.
        foreach ($attachments as $enclosure) {
          // Build an array of references to strings that need file paths and
          // URLs substituted. Only build it once and re-use it for all
          // orphaned attachments
          static $replace;
          if (!$replace) {
            $replace = array();
            foreach (array_keys($this->feeds_source_items) as $item_key) {
              foreach (array('content', 'content_body', 'content_teaser') as $source_key) {
                $replace[] = &$this->feeds_source_items[$item_key][$source_key];
              }
              // @todo Minor:  Substitute file paths & URLs in comments too.
            }
          }

          // @todo Settings:  Directory 'wordpress/orphaned-attachments'.
          $enclosure->saveFile(file_directory_path() . '/wordpress/orphaned-attachments', $replace);
        }
      }
      else {
        // Store the attachment item as an attachment to it's parent post item.
        $this->feeds_source_items[$post_id]['wp_attachments'] = array();
        foreach ($attachments as $attachment) {
          if (!isset($attachment->list)) {
            $attachment->list = $this->feeds_source_items[$post_id]['is_published'];
          }
          $this->feeds_source_items[$post_id]['wp_attachments'][] = $attachment;
        }
      }
    }
  }

  /**
   * Parse the extra mapping sources provided by this parser.
   */
  protected function parseItem() {
    static $status_map;

    // Allow sub-classes to provide additional or alternative item-filtering.
    if ($this->skipItem()) {
      // @todo Major:  It may be preferable to import attachments as nodes with
      // a node-reference to the parent.
      $this->storeAttachmentPost();
      return;
    }

    $item = array();

    // Force the same source items to be handled as "modified" every time, so
    // that target nodes are updated. This is useful for developing with Feeds
    // API, especially set-target callbacks.
    // @todo Comment out debugging code that forces items to update every time.
    // $item['random'] = time();

    $simplepie_item = $this->simplePieItem();

    $item['title'] = html_entity_decode(($title = $simplepie_item->get_title()) ? $title : $this->createTitle($simplepie_item->get_content()));
    $item['description'] = $simplepie_item->get_content();
    $item['url'] = html_entity_decode($simplepie_item->get_link());

    // Check a valid timestamp is returned (post_date_gmt can be
    // "0000-00-00 00:00:00" when the item is not published).
    if (!$timestamp = strtotime($this->elementValue('post_date_gmt') . ' UTC')) {
      // Use unix time from the feed. If no date is defined, fall back to
      // FEEDS_REQUEST_TIME.
      if (!$timestamp = $simplepie_item->get_date('U')) {
        $timestamp = FEEDS_REQUEST_TIME;
      }
    }
    $item['timestamp'] = $timestamp;

    $item['guid'] = $simplepie_item->get_id();
    // Use URL as GUID if there is no GUID.
    if (empty($item['guid'])) {
      $item['guid'] = $item['url'];
    }
    $author = $simplepie_item->get_author();
    $item['author_name'] = !empty($author->name) ? html_entity_decode($author->name) : '';

    // Allow parsing to be extended.
    $this->parseExtensions($item, $simplepie_item);
    $item['raw'] = $simplepie_item->data;

    return $item;
  }

  /**
   * Tells parseItem() whether or not to even import this item.
   */
  protected function skipItem() {
    static $filters;
    if (!$filters) {
      // @todo Settings:  Make $filters configurable so that items of other
      // types (e.g. page, attachment) or statuses (e.g. schedule) may be
      // imported.
      $filters = array(
        'post_type' => array('post'),
        'status' => array('publish', 'draft'),
      );
    }

    foreach ($filters as $name => $accepted_values) {
      // Assume filters are all single-value properties.
      if (!in_array($this->elementValue($name), $accepted_values)) {
        // The filters are ANDed.
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Stores attachment posts for processing later.
   */
  protected function storeAttachmentPost() {
    if ($this->elementValue('post_type') == 'attachment') {
      // Store attachment items for mapping back to their wp:post_parent later.
      $parent_id = $this->elementValue('post_parent');
      if (empty($this->attachment_items[$parent_id])) {
        $this->attachment_items[$parent_id] = array();
      }

      $simplepie_item = $this->simplePieItem();

      // WordPress has another XML namespace just for excerpts.  *sigh*
      if ($values = $simplepie_item->get_item_tags(WP_FEEDS_XMLNS . 'excerpt/', 'encoded')) {
        $excerpt = $values[0]['data'];
      }

      if (!$title = $simplepie_item->get_title()) {
        $title = $this->createTitle($simplepie_item->get_content());
      }
      $title = html_entity_decode($title);

      $enclosure = new FeedsWordpressEnclosure($this->elementValue('attachment_url'));
      $enclosure->page_url = html_entity_decode($this->simplePieItem()->get_link());
      $enclosure->data = array('description' => $excerpt, 'alt' => $excerpt, 'title' => $title);
      $this->attachment_items[$parent_id][] = $enclosure;
    }
  }

  /**
   * Parse the extra mapping sources provided by this parser.
   */
  protected function parseExtensions(Array &$item) {
    // Set simple straightforward single-values.
    $single_value_elements = array(
      'status',
      'post_id',
      'post_date',
      'post_date_gmt',
      'post_type',
      'post_name',
      'post_parent',
      'post_password',
      'menu_order',
      'attachment_url',
    );
    foreach ($single_value_elements as $key) {
      $item["wp_$key"] = $this->elementValue($key);
    }

    $this->parseBooleans($item);
    $this->parsePostmeta($item);
    $this->parseTags($item);
    $this->parseComments($item);

    // A Drupal-compatible published-status.
    $item['is_published'] = $this->mapStatus($item['wp_status']);

    // Get the full path for path aliases.
    if ($path = wp_feeds_url_path($item['url'])) {
      $item['path'] = $path;
    }

    $item['content'] = str_replace('<!--more-->', '<!--break-->', $item['description']);
    // @todo Minor:  The input format from the FeedsNodeProcessor configuration
    // is appropriate here.  But it is not easily accessible from here.
    $item['content_teaser'] = node_teaser($item['content'], variable_get('filter_default_format', 1));
    $remove = array($item['content_teaser'], '<!--break-->');
    $item['content_body'] = str_replace($remove, '', $item['content']);
  }

  /**
   * Various elements have simple booleans which are easy to parse all at once.
   */
  protected function parseBooleans(Array &$item) {
    // Map '1'/'open' and '0'/'closed' statuses to TRUE and FALSE respectively.
    foreach (array('comment_status', 'ping_status', 'is_sticky') as $key) {
      $value = $this->elementValue($key);
      if (isset($value)) {
        if (in_array($value, array('1', 'open'), TRUE)) {
          $item["wp_$key"] = TRUE;
        }
        else if (in_array($value, array('0', 'closed'), TRUE)) {
          $item["wp_$key"] = FALSE;
        }
        else {
          // Value should only ever be '1','open', '0' or 'closed'.
          // @todo Log this case and/or handle it better.
          dpm($value, "Bad Boolean: $key");
        }
      }
    }
  }

  /**
   * Process <wp:postmeta> elements.
   */
  protected function parsePostMeta(Array &$item) {
    $post_meta = array();
    foreach ($this->elements('postmeta') as $element) {
      $key = $element['child'][WP_FEEDS_XMLNS]['meta_key'][0]['data'];
      $value = $element['child'][WP_FEEDS_XMLNS]['meta_value'][0]['data'];
      $post_meta[$key] = $value;
    }

    $meta_sources = array(
      'email_notification',
      'geo_latitude',
      'geo_longitude',
      'geo_accuracy',
      'geo_address',
      'geo_public',
    );
    foreach ($meta_sources as $source) {
      $item["meta_$source"] = $post_meta[$source];
    }

    if (isset($post_meta['_wp_attachment_metadata'])) {
      $item['meta_attachment_metadata'] = $post_meta['_wp_attachment_metadata'];
    }

    if (isset($post_meta['_edit_lock'])) {
      $item['meta_edit_lock_date'] = $post_meta['_edit_lock'];
    }

    // @todo Major:  Consider creating a sub-class of SimplePie_Enclosure;
    // Tell SimplePie to use the sub-class instead of SimplePie_Enclosure.
    // @see SimplePie::set_enclosure_class()
    // @see http://simplepie.org/wiki/reference/start#methods

    if (isset($post_meta['enclosure'])) {
      $parts = preg_split('/[\s]+/', $post_meta['enclosure']);
      $item['meta_enclosure'] = new FeedsWordpressEnclosure($parts[0], $parts[2]);
    }
  }

  /**
   * Parse out WordPress categories and tags for this WordPress post item.
   */
  protected function parseTags(Array &$item) {
    // Process tags into "domains" (RSS-speak for vocabularies).
    $tags = array();
    if ($categories = $this->simplePieItem()->get_categories()) {
      foreach ($categories as $tag) {
        if ($domain = (string) $tag->get_scheme()) {
          if ($term = $this->cleanTaxonomyTerm($tag->term)) {
            if (!isset($tags[$domain])) {
              $tags[$domain] = array();
            }
            $tags[$domain][] = $term;
          }
        }
      }
      $item['categories'] = $tags['category'];
      $item['tags'] = $tags['tag'];
    }
  }

  /**
   * Process comments.
   */
  protected function parseComments(Array &$item) {
    $this->trackbacks = array();
    $this->comments = array();

    if ($elements = $this->elements('comment')) {
      foreach ($elements as $element) {
        $this->parseComment($element);
      }
    }

    $item['comments'] = $this->comments;

    // @todo Minor:  Implement a set-target callback for trackbacks.
    // @see wordpress_import_process_post_comments()
    $item['trackbacks'] = $this->trackbacks;
  }

  /**
   * Parse a WordPress comment to be more like a Drupal comment.
   */
  protected function parseComment(Array $element) {
    $wp_comment = new stdClass();
    foreach ($element['child'][WP_FEEDS_XMLNS] as $key => $values) {
      $key = str_replace('comment_', '', $key);
      $wp_comment->{$key} = $values[0]['data'];
    }
    $wp_comment->id = (Integer) $wp_comment->id;
    $wp_comment->parent = (Integer) $wp_comment->parent;

    switch ($wp_comment->approved) {
      case '1':
        $status = COMMENT_PUBLISHED;
        break;
      case '0':
        $status = COMMENT_NOT_PUBLISHED;
        break;
      case 'spam':
        return;
      default:
        // The value should only ever be '0', '1' or 'spam'.
        // @todo Log this case and/or handle it better.
        dpm($wp_comment, 'Invalid wp_comment->approved status');
        return;
    }

    // 29 characters in length is hardcoded in Drupal's _comment_submit() too.
    $subject = truncate_utf8(trim(strip_tags(check_markup($wp_comment->content, $format))), 29, TRUE);
    $name = strip_tags($wp_comment->author);

    if (!$timestamp = strtotime($wp_comment->date_gmt . ' UTC')) {
      // Do not let $timestamp be parsed as the unix epoch of 1979.
      $timestamp = FEEDS_REQUEST_TIME;
    }

    switch ($wp_comment->type) {
      case 'trackback':
      case 'pingback':
        // @todo Settings:  Trackbacks feature.
        // @todo Minor:  Move to trackback project.
        if (module_exists('trackback')) {
          // Implement the target callback.
          $trackback = array(
            'created' => $timestamp,
            'site' => $wp_comment->author_IP,
            'name' => $name,
            'subject' => $subject,
            'url' => $wp_comment->author_url,
            'excerpt' => $wp_comment->content,
            // @todo Minor:  Use a constant from trackback.module.
            'status' => 1,
          );
          $this->trackbacks[$wp_comment->id] = $trackback;
        }
        break;
      default:
        $comment = array(
          'wp_parent' => $wp_comment->parent,
          'comment' => $wp_comment->content,
          'subject' => $subject,
          'name' => $name,
          'mail' => $wp_comment->author_email,
          'homepage' => $wp_comment->author_url,
          'timestamp' => $timestamp,
          'hostname' => $wp_comment->author_IP,
          'status' => $status,
        );
        $this->comments[$wp_comment->id] = $comment;
        break;
    }
  }

  /**
   * Maps a WordPress post status to Drupal published status.
   *
   * @param $status String
   *    The status to map.
   * @return Boolean
   *    TRUE if published.  FALSE if not.  VOID if unknown.
   */
  protected function mapStatus($status) {
    static $status_map;
    if (!$status_map) {
      $status_map = array(
        // @todo Minor:  Add support for a node-scheduling contrib module.
        'schedule' => TRUE,

        // If status is "inherit", it needs to be set when attachment items are
        // mapped back into their parent post items in attachAttachmentPosts().
        // Do not set it for now.
        // 'inherit' => NULL,

        'publish' => TRUE,
        'draft' => FALSE,
        'auto-draft' => FALSE,
        'trash' => FALSE,
      );
    }

    if ($status_map[$status]) {
      return $status_map[$status];
    }
  }

  /**
   * Gets one value for one tag in the current SimplePie_Item.
   *
   * @param $tag_name String
   *    The name of the tag to be retrieved excluding the namespace prefix.
   *    E.g. 'is_sticky', not 'wp:post_sticky'.
   * @return String
   *    The value of the textnode of that element.  It is always formatted as a string.
   */
  protected function elementValue($tag_name) {
    if ($value = $this->elements($tag_name)){
      return $value[0]['data'];
    }
  }

  /**
   * Gets the tags/elements of type $tag_name in the current SimplePie_Item.
   *
   * @param $tag_name String
   *    The name of the tag to be retrieved excluding the namespace prefix.
   *    E.g. 'is_sticky', not 'wp:post_sticky'.
   * @return Array
   *    A nested array of data and attributes.  Put it through dpm()!
   */
  protected function elements($tag_name) {
    return $this->simplePieItem()->get_item_tags(WP_FEEDS_XMLNS, $tag_name);
  }

  /**
   * Gets the current SimplePie_Item.
   */
  protected function simplePieItem() {
    return $this->simplepie_items[$this->current_item_index];
  }
}


/**
 * Extends FeedsEnclosure to support FileField properties & handle redirects.
 *
 * @todo Major:  Consider rewriting parse* methods in sub-classes of SimplePie classes.
 * @see "Extending Classes" http://simplepie.org/wiki/reference/start#methods
 * @see http://drupal.org/node/622700
 */
class FeedsWordpressEnclosure extends FeedsEnclosure {
  /**
   * @property $page_url
   *
   * The URL of the HTML page for this file (not the file URL itself).  Use
   * redirectPageTo() to create a redirect from $page_url to $redirect.
   */
  public $page_url;

  /**
   * @property $page_url
   *
   * Array of alt, title and description strings.
   */
  public $data;

  /**
   * Constructor.
   *
   * Additionally, callers might like to setthe properties page_url and data.
   *
   * @param $file_url String
   *    The URL of the file to be saved.
   * @param $mime String
   *    The MIME type of the file.
   */
  public function __construct($file_url, $mime = 'application/octet-stream') {
    parent::__construct($file_url, $mime);
  }

  /**
   * Saves the files from a remote location to disk and to the files table.
   *
   * @param $destination String
   *    The directory to save the file in, without the trailing slash.
   * @param $replace Array
   *    References to strings in which to substitute the file's old path and
   *    URL with the file's new path and URL.
   * @param $account Object
   *    User account (if any) which owns these files.
   * @return File array
   *    A file array like what field_file_load() returns.
   */
  public function saveFile($destination, Array &$replace, $account = NULL) {
    // Check if the file is already on the filesystem.
    // @todo Minor:  Maintain a map of original:retrieved files so that files
    // with the same path but different domains do not conflict, and so that
    // path_redirect need not be a dependency.
    $sql = "SELECT fid FROM files f INNER JOIN path_redirect r ON f.filepath = r.redirect WHERE r.source = '%s' LIMIT 1";
    if ($fid = db_result(db_query($sql, wp_feeds_url_path($this->getValue())))) {
      // @todo Minor:  Is it a problem that this stops files from being
      // re-retrieved (by http) from their original source?
      // Keep the file from being deleted from the filefield, filesystem and
      // the files table.
      $return = field_file_load($fid);
    }
    else {
      // Check the target directory exists, or create it.
      if (!$this->checkDirectory($destination)) {
        drupal_set_message(t('The directory %dir could not be created.  Please check the permissions in your files directory %files_dir.', array('%dir' => $destination, '%files_dir' => file_directory_path())), 'error');
        return;
      }

      // Save the file to disk.
      $return = field_file_save_file($this->getFile(), array(), $destination, $account);
    }

    // Do not store redirects to temp files.  Temp paths can get to here if the
    // files directory does not exist and/or permissions are incorrect.
    $temp = file_directory_temp();
    if (($substr = substr($return['filepath'], 0, strlen($temp))) !== $temp) {
      $this->saveRedirect($return['filepath']);
      $this->substitute($return['filepath'], $replace);
    }

    return $return;
  }

  /**
   * Saves a redirect from the file's HTML page ($this->page_url) to $redirect.
   *
   * @param $redirect String
   *    The Drupal path to redirect to.
   */
  public function redirectPageTo($redirect) {
    if ($source = wp_feeds_url_path($this->page_url)) {
      // Check that a redirect does not already exist for this path.
      if (!path_redirect_load_by_source($source)) {
        // A permanent redirect.
        $type = 301;
        // Save the redirect.
        path_redirect_save(compact('source', 'redirect', 'type'));
      }
    }
  }

  /**
   * Tries to creates the destination directory if it does not exist.
   *
   * @param $destination String
   *    The destination directory.
   * @return Boolean
   *    TRUE if the $destination directory exists.
   *    FALSE if it does not exist.
   */
  protected function checkDirectory($destination) {
    if (!file_check_directory($destination, TRUE)) {
      // Check the parent directory exists, or create it.
      $parent_directory = substr($destination, 0, strrpos($destination, '/'));
      if (!file_check_directory($parent_directory, TRUE)) {
        // Give up.
        return FALSE;
      }
      // Try again with the target directory.
      else if (!file_check_directory($destination, TRUE)) {
        // Give up.
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Saves a redirect for the file from it's old path to it's new path.
   */
  protected function saveRedirect($filepath) {
    // Get the path that WordPress used the old domain.
    if ($source = wp_feeds_url_path($this->getValue())) {
      // Check that a redirect does not already exist for this path.
      if (!path_redirect_load_by_source($source)) {
        // Get the full Drupal path to redirect to.
        if ($redirect = file_create_path($filepath)) {
          // A permanent redirect.
          $type = 301;
          // Save the redirect.
          path_redirect_save(compact('source', 'redirect', 'type'));
        }
      }
    }
  }

  /**
   * Replaces old file paths & URLs with new file paths & URLs in $subjects.
   */
  protected function substitute($filepath, Array &$subjects) {
    $old_url = $this->getValue();
    $new_url = file_create_url($filepath);
    $old_path = wp_feeds_url_path($this->getValue());
    $new_path = file_create_path($filepath);

    // str_replace() accepts an array for the $search (1st) and $replace (2nd)
    // paramters.  However the order that str_replace() iterates over these is
    // not guaranteed.  In this instance, the order is important.

    // str_replace() accepts an array for the $subject (3rd) parameter.
    // However it is important that the _referenced_ value of each element in
    // $subjects is modified, and not $subjects itself.

    foreach (array_keys($subjects) as $key) {
      // First; Replace full URLs.
      $subjects[$key] = str_replace($old_url, $new_url, $subjects[$key]);
      // @todo Minor:  Do references to the same file path on other domains also need to be replaced?
      // Second; Replace paths that exclude http:// and the domain.
      $subjects[$key] = str_replace($old_path, $new_path, $subjects[$key]);
    }
  }
}
